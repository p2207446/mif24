### Question 1
Calculer le nombre de notes de chaque type.

```mongodb
db.grades.aggregate([
  {$unwind : "$scores"},
  {$group : {_id : "$scores.type", nombre : {$sum : 1}}}
])
```

### Question 2

Donner pour chaque matière, la meilleure note d'examen.

```mongodb

```

### Question 3

Donner les dix villes les plus peuplées.

```mongodb

```
### Question 4

Donner la population moyenne des villes pour chaque état.

```mongodb

```
### Question 5

Donner pour chaque matière, les étudiants ayant une note d'examen supérieure à 50.

```mongodb

```
### Question 6
Donner pour chaque étudiant et chaque matière sa note générale. La note générale est la moyenne de chaque type de note. S'il y a plusieurs notes d'un même type, on prendra d'abord la moyenne de ce type avant de l'utiliser pour calculer la moyenne avec les autres types.

```mongodb

```

### Question 7

Donner, pour chaque matière le type d'épreuve la mieux réussie. Le type d'épreuve la mieux réussie est celui ayant la meilleure moyenne, calculée sur l'ensemble des notes de ce type pour cette matière.

```mongodb

```